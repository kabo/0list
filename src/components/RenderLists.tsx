import Collapse from 'antd/lib/collapse'
import * as R from 'ramda'
import React from 'react'
import RenderList from './RenderList'
import { IList, IListItem, IRenderListsProps } from '../types'

const { Panel } = Collapse

export default React.memo(({ lists, checkItem, deleteItem, addItem }: IRenderListsProps): JSX.Element => {
  // console.log('Rendering RenderLists') // eslint-disable-line fp/no-unused-expression
  return (
    <Collapse bordered={false} destroyInactivePanel={true}>
      {R.map((list: IList) => (
        <Panel header={list.name} key={list.i}>
          <RenderList
            key={list.i}
            list={list}
            checkItem={({ item, checked }: { readonly item: IListItem, readonly checked: boolean }) => checkItem({ list, item, checked })}
            deleteItem={(item: IListItem) => deleteItem({ list, item })}
            addItem={(name: string) => addItem({ list, name })}
          />
        </Panel>
      ), lists)}
    </Collapse>

  )
})
