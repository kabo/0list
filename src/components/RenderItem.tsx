import Button from 'antd/lib/button'
import Checkbox from 'antd/lib/checkbox'
import List from 'antd/lib/list'
import Popconfirm from 'antd/lib/popconfirm'
import React from 'react'
import { IRenderItemProps } from '../types'

export default React.memo(({ item, checkItem, deleteItem }: IRenderItemProps): JSX.Element => {
  // console.log('Rendering Item') // eslint-disable-line fp/no-unused-expression
  return (
    <List.Item
      actions={[
        <Popconfirm
          key="del"
          title="Delete?"
          okText="Delete!"
          cancelText="Cancel"
          onConfirm={() => deleteItem(item)}
        >
          <Button
            icon="delete"
            className="list-item-delete"
          />
        </Popconfirm>,
      ]}
    >
      <div
        onClick={() => checkItem({ item, checked: !item.checked })}
        className={`list-item list-item-${item.checked ? 'checked' : 'unchecked'}`}
      >
        <Checkbox
          checked={item.checked}
          onChange={() => checkItem({ item, checked: !item.checked })}
          className="list-item-checkbox"
        />
        {item.name}
      </div>
    </List.Item>
  )
})
