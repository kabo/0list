import AutoComplete from 'antd/lib/auto-complete'
import * as R from 'ramda'
import React, { useCallback } from 'react'
import { useEventCallback } from 'rxjs-hooks'
import { IItemAdderProps } from '../types'

const itemNames = R.memoizeWith(R.identity as (a: any) => any, R.map(R.prop('name')))

export default React.memo(({ items, checkItem, addItem }: IItemAdderProps): JSX.Element => {
  // console.log('Rendering ItemAdder') // eslint-disable-line fp/no-unused-expression
  const [ setNewItemText, newItemText ]: [ any, string ] = useEventCallback<any, string>(
    R.identity,
    ''
  ),
    setEmptyItemText = useCallback((a?: any) => setTimeout(() => setNewItemText(''), 1), [ setNewItemText ]),
    itemSelected = useCallback((name: any) => {
      return typeof name !== 'string' ? setNewItemText('')
        : name.startsWith('Create ') ? setEmptyItemText(addItem(name.substring(7)))
          : setEmptyItemText(checkItem({
            checked: false,
            item: R.find(R.propEq('name', name), items),
          }))
    }, [ addItem, checkItem, items, setEmptyItemText, setNewItemText ]),
    dataSource = itemNames(items)
  return (
    <div>
      <AutoComplete
        dataSource={R.append(`Create ${newItemText}`, dataSource)}
        filterOption={(inputValue, option) => R.contains(
          R.toLower(inputValue),
          R.toLower(R.pathOr('', [ 'props', 'children' ], option))
        )}
        onSelect={itemSelected}
        onChange={setNewItemText}
        value={newItemText}
        placeholder="Add new item..."
      />
    </div>
  )
})
