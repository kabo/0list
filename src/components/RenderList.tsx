import List from 'antd/lib/list'
import React from 'react'
import ItemAdder from './ItemAdder'
import RenderItem from './RenderItem'
import { IListItem, IRenderListProps } from '../types'

export default React.memo(({ list, checkItem, deleteItem, addItem }: IRenderListProps): JSX.Element => {
  // console.log('Rendering RenderList') // eslint-disable-line fp/no-unused-expression
  // TODO: make list a virtualized list with react-virtualized
  return (
    <div>
      {/* <Button icon="delete" onClick={() => deleteList({list})} /> */}
      <ItemAdder
        items={list.items}
        checkItem={checkItem}
        addItem={addItem}
      />
      <List
        bordered={false}
        dataSource={list.items}
        rowKey="i"
        renderItem={(item: IListItem) =>
          <RenderItem
            item={item}
            deleteItem={deleteItem}
            checkItem={checkItem}
          />
        }
      />
    </div>
  )
})
