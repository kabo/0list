/* eslint-disable fp/no-let,fp/no-nil,fp/no-rest-parameters,fp/no-mutation,better/no-ifs */

export default (fn: any) => {
  let lastResult: any,
    lastArgs = '',
    calledOnce = false
  return (...args: any[]) => {
    const curArgs = JSON.stringify(args)
    if (calledOnce && curArgs === lastArgs) {
      return lastResult
    }
    calledOnce = true
    lastArgs = curArgs
    lastResult = fn(...args)
    return lastResult
  }
}
