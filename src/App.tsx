import { useMutation, useQuery } from '@apollo/react-hooks'
import { decode as base64decode, encode as base64encode } from '@stablelib/base64'
import { decode as utf8decode, encode as utf8encode } from '@stablelib/utf8'
import Button from 'antd/lib/button'
import Col from 'antd/lib/col'
import Input from 'antd/lib/input'
import ConfigProvider from 'antd/lib/config-provider'
import enUS from 'antd/lib/locale-provider/en_US'
import Popover from 'antd/lib/popover'
import Row from 'antd/lib/row'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { setContext } from 'apollo-link-context'
import { HttpLink } from 'apollo-link-http'
import gql from 'graphql-tag'
import * as R from 'ramda'
import React, { useCallback, useMemo } from 'react'
import { QRCode } from 'react-qr-svg'
import { fromEventPattern } from 'rxjs'
import { useEventCallback, useObservable } from 'rxjs-hooks'
import { map } from 'rxjs/operators'
import nacl from 'tweetnacl'
import memoizeOne from './memoizeOne'
import RenderLists from './components/RenderLists'
import { IEncryptedList, IList, ILists, IListConfig, IListConfigs, IListItem } from './types'

const
  authLink = setContext((_, { headers }) => {
    const apikey = process.env.REACT_APP_APPSYNC_API_KEY
    return {
      headers: {
        ...headers,
        'x-api-key': apikey || '',
      },
    }
  }),
  // eslint-disable-next-line better/no-new
  httpLink = new HttpLink({
    uri: `https://${process.env.REACT_APP_APPSYNC_ENDPOINT}`,
  }),
  // eslint-disable-next-line better/no-new
  client = new ApolloClient({ link: authLink.concat(httpLink), cache: new InMemoryCache() }),
  CREATE_LIST = gql`
    mutation CreateList($input: CreateListInput!) {
      createList(input: $input) {
        i
      }
    }
  `,
  UPDATE_LIST = gql`
    mutation UpdateList($i: ID!, $input: UpdateListInput!) {
      updateList(i: $i, input: $input) {
        i
        n
        data
      }
    }
  `,
  /* GET_LIST = gql`
    query GetList($i: ID!) {
      getList(i: $i) {
        i
        n
        data
      }
    }
  `, */
  GET_LISTS = gql`
    query GetLists($ids: [ID]!) {
      getLists(ids: $ids) {
        i
        n
        data
      }
    }
  `

interface IListDb {
  readonly i: string
  readonly n: string // nonce
  readonly data: string
}

type IListDbs = ReadonlyArray<IListDb>

interface IHashState {
  readonly l: IListConfigs
}

interface IEncryptionArgs {
  readonly data: string
  readonly key: string
}

interface IDecryptionArgs {
  readonly data: string
  readonly key: string
  readonly nonce: string
}

const rem = (handler: any) => window.removeEventListener('hashchange', handler),
  add = (handler: any) => {
    return window.addEventListener('hashchange', handler)
  },
  splitOnHash = (str: string) => str.split('#', 2),
  getHash = R.pipe<string, ReadonlyArray<string>, string, string>(
    splitOnHash,
    R.last,
    decodeURI
  ),
  getHashState = (url: string): IHashState => {
    try {
      return JSON.parse(getHash(url))
    } catch (err) {
      console.warn(err) // eslint-disable-line fp/no-unused-expression
    }
    return { l: [] }
  },
  // eslint-disable-next-line fp/no-nil,better/explicit-return
  setHashState = (hashState: IHashState) => {
    // eslint-disable-next-line fp/no-mutation
    window.location.hash = JSON.stringify(hashState)
  },
  encrypt = ({ data, key }: IEncryptionArgs) => {
    const udata = utf8encode(data),
      ukey = base64decode(key),
      unonce = nacl.randomBytes(nacl.secretbox.nonceLength),
      encrypted = nacl.secretbox(udata, unonce, ukey)
    return { data: base64encode(encrypted), nonce: base64encode(unonce) }
  },
  decrypt = ({ data, nonce, key }: IDecryptionArgs) => {
    const udata = base64decode(data),
      unonce = base64decode(nonce),
      ukey = base64decode(key),
      decrypted = nacl.secretbox.open(udata, unonce, ukey) // eslint-disable-line security/detect-non-literal-fs-filename

    return decrypted && utf8decode(decrypted)
  },
  encryptList = (list: IList) => {
    const { i, k, name, items } = list,
      toBeEncrypted = JSON.stringify({ name, items }),
      { data, nonce } = encrypt({ data: toBeEncrypted, key: k })
    return { i, k, n: nonce, data } as IEncryptedList
  },
  decryptList = (list: IEncryptedList) => {
    const { i, k, n, data } = list,
      jsonString = decrypt({ data, key: k, nonce: n })
    return !jsonString ? false
      : {
        i,
        k,
        ...R.pick([ 'name', 'items' ], JSON.parse(jsonString)),
      } as IList
  },
  createList = (gqlFunc: any, name: string) => {
    const k = base64encode(nacl.randomBytes(nacl.secretbox.keyLength)),
      // items: IListItems = [],
      toBeEncrypted = JSON.stringify({ name, items: [] }),
      { data, nonce } = encrypt({ data: toBeEncrypted, key: k })
    // console.log({ k })
    return gqlFunc({ variables: { input: { data, n: nonce } } })
      .then((resp: any) => {
        // console.log({ resp })
        const hashState = getHashState(window.location.href),
          conf = { i: resp.data.createList.i, k },
          updatedHashState = { ...hashState, l: R.append(conf, hashState.l) }
        return setHashState(updatedHashState)
      })
  },
  decryptDbResponse = memoizeOne((dbResponses: IListDbs, listConfigs: IListConfigs) =>
    R.reduce<IListConfig, ILists>(
      (acc: ILists, l: IListConfig) =>
        R.pipe<IListDbs, IListDb | undefined, ILists>(
          R.find(R.eqProps('i', l)),
          R.ifElse( // discards listConfigs that don't have a corresponding db response
            R.isNil,
            R.always(acc),
            R.pipe<IListDb, IEncryptedList, IList | boolean, ILists>(
              R.mergeRight(l),
              decryptList,
              R.ifElse( // discards lists that were undecryptable
                Boolean,
                R.flip(R.append)(acc),
                R.always(acc)
              )
            )
          )
        )(dbResponses),
      [],
      listConfigs
    )),
  equals = R.curry((a: any, b: any) => a === b)

// tslint:disable-next-line:max-line-length
// http://localhost:3000/#{"l":[{"i":"024cd831-7cb6-498e-a4b6-5f1dfba8ca99","k":"s7tNnQxn4KBPjlQ+qQujDL42h8t5t1MkjHb+53387qA="}]}
// tslint:disable-next-line:max-line-length
// http://localhost:3000/#{%22l%22:[{%22i%22:%22024cd831-7cb6-498e-a4b6-5f1dfba8ca99%22,%22k%22:%22s7tNnQxn4KBPjlQ+qQujDL42h8t5t1MkjHb+53387qA=%22},{%22i%22:%22908d6980-5fc8-4657-818b-4d5a8788971f%22,%22k%22:%2218GQASebLW0AaygAB4RuKdjO5u9XDcPSL5VDOmRIg3A=%22}]}

export default (): JSX.Element => {
  const
    url = useObservable(
      () =>
        fromEventPattern(add, rem).pipe(
          map((loc: any) => {
            // console.log({ loc })
            return loc.newURL
          })
        ),
      window.location.href
    ),
    [ checkItem ]: [ any, any ] = useEventCallback<any, any>(
      (event$) => event$.pipe(
        map(({ item, list, checked, refetch }) => {
          const newItem = { ...item, checked },
            newList = {
              ...list,
              items: R.sortBy<IListItem>(
                R.prop('checked'),
                R.update(R.findIndex(equals(item), list.items), newItem, list.items)
              ),
            },
            { i, n, data } = encryptList(newList)
          // console.log({ newItem, newList, n, data })
          return client.mutate({
            mutation: UPDATE_LIST,
            variables: { i, input: { n, data } },
          })
            .then(() => refetch())
        })
      ),
      []
    ),
    [ deleteItem ]: [ any, any ] = useEventCallback<any, any>(
      (event$) => event$.pipe(
        map(({ item, list, refetch }) => {
          const newList = { ...list, items: R.remove(R.findIndex(equals(item), list.items), 1, list.items) },
            { i, n, data } = encryptList(newList)
          // console.log({ newList, n, data })
          return client.mutate({
            mutation: UPDATE_LIST,
            variables: { i, input: { n, data } },
          })
            .then(() => refetch())
        })
      ),
      []
    ),
    [ addItem ]: [ any, any ] = useEventCallback<any, any>(
      (event$) => event$.pipe(
        map(({ name, list, refetch }) => {
          const item: IListItem = { name, checked: false },
            newList: IList = { ...list, items: [ item, ...list.items ] },
            { i, n, data } = encryptList(newList)
          // console.log({ item, newList, n, data })
          return client.mutate({
            mutation: UPDATE_LIST,
            variables: { i, input: { n, data } },
          })
            .then(() => refetch())
        })
      ),
      []
    ),
    [ setShowQrCode, showQrCode ]: [ any, boolean ] = useEventCallback<any, boolean>(
      R.identity,
      false
    ),
    [ setNewListText, newListText ]: [ any, string ] = useEventCallback<any, string>(
      R.identity,
      ''
    ),
    setNewListTextFromTargetValue = (e: any) => setNewListText(e.target.value),
    setEmptyListText = (a?: any) => setTimeout(() => setNewListText(''), 1),
    hashState = useMemo(() => getHashState(url), [ url ]),
    { loading, /* error, */ data, refetch }: { loading: boolean, data: any, refetch: any } = useQuery(GET_LISTS, {
      skip: hashState.l.length < 1,
      pollInterval: 60000,
      variables: { ids: R.map(R.prop('i'), hashState.l) },
      client,
    }),
    [ gqlFunc ] = useMutation(CREATE_LIST, { client }),
    createNewList = (e: any) => setEmptyListText(createList(gqlFunc, e.target.value)),
    lists = useMemo(() => data?.getLists ? decryptDbResponse(data.getLists, hashState.l) : [], [ data, hashState.l ]),
    handleQueryResult = useCallback(() => {
      // console.log({ loading, error, data })
      // eslint-disable-next-line better/no-ifs
      if (!data) {
        return (<div className="side-padding">No list, create one below.</div>)
      }
      // eslint-disable-next-line better/no-ifs
      if (loading) {
        return (<div className="side-padding">Loading...</div>)
      }
      return (
        <RenderLists
          lists={lists}
          addItem={({ name, list }: { readonly name: string, readonly list: IList }) => addItem({ list, refetch, name })}
          checkItem={({ list, item, checked }: { readonly list: IList, readonly item: IListItem, readonly checked: boolean }) => checkItem({ list, item, checked, refetch })}
          deleteItem={({ list, item }: { readonly list: IList, readonly item: IListItem }) => deleteItem({ list, item, refetch })}
        />
      )
    }, [ data, loading, lists, addItem, refetch, checkItem, deleteItem ])
  return (
    <ConfigProvider locale={enUS}>
      <div className="App">
        <header className="App-header side-padding">
          <Row>
            <Col span={12} className="app-header-logo">
              0list.net
            </Col>
            <Col span={12} className="app-header-menu">
              By <a href="https://kabohub.co.nz/" target="_blank" rel="noopener noreferrer">KaboHub</a>
              &nbsp;|
                &nbsp;<a href="https://gitlab.com/kabo/0list" target="_blank" rel="noopener noreferrer">Source</a>
            </Col>
          </Row>
        </header>
        <Row className="lists-block">
          <Col span={24}>
            {useMemo(() => handleQueryResult(), [ handleQueryResult ])}
          </Col>
        </Row>
        <Row className="new-list-block side-padding">
          <Col span={24}>
            <div>
              <Input
                placeholder="New list..."
                value={newListText}
                onChange={setNewListTextFromTargetValue}
                onPressEnter={createNewList}
              />
            </div>
          </Col>
        </Row>
        <Row className="app-menu side-padding">
          <Col span={24} className="align-right">
            <Popover
              content={<div><QRCode value={url} className="qrcode-popover" cellClassPrefix="qr-svg" /></div>}
              trigger="click"
              visible={showQrCode}
              onVisibleChange={setShowQrCode}
            >
              <Button>{showQrCode ? 'Hide' : 'Show'}&nbsp;QRCode for URL</Button>
            </Popover>
          </Col>
        </Row>
      </div>
    </ConfigProvider>
  )
}
