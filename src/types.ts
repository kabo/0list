export interface IListConfig {
  readonly i: string // id
  readonly k: string // key
}

export type IListConfigs = ReadonlyArray<IListConfig>

export interface IEncryptedList extends IListConfig {
  readonly data: string
  readonly n: string // nonce
}

// type IEncryptedLists = ReadonlyArray<IEncryptedList>

export interface IListItem {
  readonly name: string
  readonly checked: boolean
}

export type IListItems = IListItem[]

export interface IList extends IListConfig {
  readonly name: string
  readonly items: IListItems
}

export type ILists = ReadonlyArray<IList>

export interface IRenderItemProps {
  readonly item: IListItem
  readonly deleteItem: any
  readonly checkItem: any
}

export interface IRenderListProps {
  readonly list: IList
  readonly checkItem: any
  readonly deleteItem: any
  readonly addItem: any
}

export interface IItemAdderProps {
  readonly items: IListItems
  readonly addItem: any
  readonly checkItem: any
}

export interface IRenderListsProps {
  readonly lists: ILists
  readonly checkItem: any
  readonly deleteItem: any
  readonly addItem: any
}
