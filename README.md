# 0list

Zero-knowledge, encrypted shopping lists.

## Run it locally

```shell
git clone https://gitlab.com/kabo/0list.git
cd 0list
yarn install
yarn start
```

## todo

- [x] list-id (uuid) and encryption (tweetnacl) key in the url
- [x] checked items get moved below unchecked items automatically
- [x] ability to switch between lists when multiple list-ids are in the url (collapse?)
- [x] aws appsync, ~subscribe to lists~ poll and refetch lists
- [x] QRCode for current URL to quickly get url to cell phone
- [x] when entering names to add items, search for existing items in that list, allow click on existing item to un-check that item (clears name input)
- [ ] make it pretty
- [x] improve [dataskydd](https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Ftest.0list.net)
  - [x] HSTS
  - [x] CSP
  - [x] Referrer policy
  - [x] HTTP headers
- [ ] ability to rename items
- [ ] themes (light, dark, etc.)
- [ ] ability to delete list
- [ ] ability to remove list from view and url
- [ ] ability to add list to view and url
- [ ] ability to rename lists
- [ ] group list by category?
- [ ] duplicate list
- [ ] export list
- [ ] import list
- [ ] add license checker to pipeline
- [ ] FAQ similar to [0bin](https://0bin.net/faq/)

## License

0list is licensed under the terms of the MIT license.

